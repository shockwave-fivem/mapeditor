import { JSDOM } from 'jsdom';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

//------------------------------------------------------------------------------
// Configure Enzyme
// See: https://airbnb.io/enzyme/docs/installation/react-16.html

configure({
  adapter: new Adapter()
});

//------------------------------------------------------------------------------
// Configure JSDOM
// See: https://github.com/airbnb/enzyme/blob/master/docs/guides/jsdom.md

const baseHtml = '<!doctype html><html><body></body></html>';
const { window } = new JSDOM(baseHtml);

// Basic DOM setup
global.window = window;
global.document = window.document;
global.navigator = { userAgent: 'node.js' };
global.requestAnimationFrame = callback => setTimeout(callback, 0);
global.cancelAnimationFrame = id => clearTimeout(id);

// Copy the DOM into the global scope
Object.defineProperties(global, {
  ...Object.getOwnPropertyDescriptors(window),
  ...Object.getOwnPropertyDescriptors(global)
});
