import { combineReducers, createStore } from 'redux';
import DevTools from '../modules/core/containers/DevTools';

import core from '../modules/core/reducer';
import menu from '../modules/menu/reducer';
import browser from '../modules/browser/reducer';
import toolbar from '../modules/toolbar/reducer';

const rootReducer = combineReducers({
  core,
  menu,
  browser,
  toolbar
});

const enhancer = DevTools.instrument({ maxAge: 50 });

export default () => createStore(rootReducer, enhancer);
