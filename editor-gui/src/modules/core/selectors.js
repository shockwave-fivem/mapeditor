import { createSelector } from 'reselect';

export const getAppState = state => state.core;

export const isGuiVisible = createSelector(
  getAppState,
  app => app.isVisible
);

export const isGuiActive = createSelector(
  getAppState,
  app => app.isActive
);
