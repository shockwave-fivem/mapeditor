import { createSelector } from 'reselect';
import { isGuiVisible as getIsGuiVisible } from '../core/selectors';

export const getToolbarState = state => state.toolbar;
export const isToolbarVisible = createSelector(
  getToolbarState,
  getIsGuiVisible,
  (toolbar, isGuiVisible) => (
    isGuiVisible &&
    toolbar.isVisible
  )
);
