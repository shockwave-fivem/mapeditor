import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import { connect } from 'react-redux';

import { isToolbarVisible } from '../selectors';
import { isGuiActive } from '../../core/selectors';

import Toolbar from '../components/Toolbar';
import Button from './Button';

import { setMenuVisible } from '../../menu/actions';
import { isMenuVisible } from '../../menu/selectors';

import { setBrowserVisible } from '../../browser/actions';
import { isBrowserVisible } from '../../browser/selectors';

// ----------------------------------------------------------------------------

// TODO: Maybe save in state to be able to add and remove buttons, and make
// them orderable? Would require a pure type for onClick and isActive.
const buttons = [{
  key: shortid.generate(),
  icon: 'menu',
  setActive: setMenuVisible,
  isActive: isMenuVisible
}, {
  key: shortid.generate(),
  icon: 'cube-outline',
  setActive: setBrowserVisible,
  isActive: isBrowserVisible
}, {
  key: shortid.generate(),
  icon: 'dots-horizontal',
  setActive: active => ({ type: 'UNKNOWN', active }),
  isActive: () => false
}];

// ----------------------------------------------------------------------------

export const ToolbarButton = (props) => {
  const {
    key,
    isActive
  } = props;

  const icon = !props.isActive()
    ? props.icon
    : 'close';

  const onClick = () => {};

  return (
    <Button
      key={key}
      icon={icon}
      isActive={isActive}
      onClick={onClick}
    />
  );
};

// ----------------------------------------------------------------------------

export const ToolbarComponent = ({ isActive, isVisible }) => (
  <Toolbar isActive={isActive} isVisible={isVisible}>
    {buttons.map(ToolbarButton)}
  </Toolbar>
);

ToolbarComponent.propTypes = {
  isActive: PropTypes.bool.isRequired,
  isVisible: PropTypes.bool.isRequired
};

export default connect(state => ({
  isActive: isGuiActive(state),
  isVisible: isToolbarVisible(state)
}))(ToolbarComponent);
