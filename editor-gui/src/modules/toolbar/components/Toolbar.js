import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 0;
  padding: 16px;
  transition: opacity 100ms ease-out;

  ${props => props.left && 'left: 0;'}
  ${props => props.right && 'right: 0;'}
  ${props => !props.isActive && 'opacity: 0.5;'}
`;
