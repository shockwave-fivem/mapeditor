import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { WindowProvider } from './components/Window';

import configureStore from './store/configureStore';
import theme from './theme';

import App from './modules/core/containers/App';

ReactDOM.render(
  <Provider store={configureStore()}>
    <ThemeProvider theme={theme}>
      <WindowProvider>
        <App />
      </WindowProvider>
    </ThemeProvider>
  </Provider>,
  document.querySelector('#root')
);
