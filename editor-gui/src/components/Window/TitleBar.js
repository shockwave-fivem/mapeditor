import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Icon from '../Icon';

// ----------------------------------------------------------------------------

const IconComponent = props => (
  <div className={props.className}>
    <Icon name={props.name} />
  </div>
);

IconComponent.propTypes = {
  className: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

// ----------------------------------------------------------------------------

const CloseComponent = props => (
  <button
    type="button"
    className={props.className}
    onClick={props.onClick}
  >
    <Icon name="close" />
  </button>
);

CloseComponent.propTypes = {
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};


// ----------------------------------------------------------------------------

export const TitleText = styled.div`
  height: 30px;
  line-height: 30px;
  margin-left: 30px;
  margin-right: 30px;
  color: white;
  text-align: center;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const TitleIcon = styled(IconComponent)`
  position: absolute;
  left: 0;
  top: 0;
  color: white;
  margin: 4px;
  padding: 0;
  height: 22px;
  width: 22px;
  line-height: 22px;
  text-align: center;
  font-size: inherit;
`;

export const TitleClose = styled(CloseComponent)`
  position: absolute;
  right: 0;
  top: 0;
  border: none;
  color: white;
  margin: 4px;
  padding: 0;
  height: 22px;
  width: 22px;
  line-height: 22px;
  text-align: center;
  border-radius: 100%;
  background: none;
  font-size: inherit;
  outline: none;

  :hover {
    background-color: rgba(0, 0, 0, 0.25);
  }
`;

// ----------------------------------------------------------------------------

const Component = props => (
  <div className={props.className}>
    <TitleText className={props.handle}>{props.children}</TitleText>
    {props.icon && <TitleIcon name={props.icon} className={props.handle} />}
    {props.onClose && <TitleClose onClick={props.onClose} />}
  </div>
);

Component.propTypes = {
  className: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  handle: PropTypes.string.isRequired,
  icon: PropTypes.string,
  onClose: PropTypes.func
};

Component.defaultProps = {
  icon: null,
  onClose: null
};

export default styled(Component)`
  position: relative;
  width: 100%;
  height: 30px;
`;
