import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Draggable from 'react-draggable';

import Panel from './Panel';
import TitleBar from './TitleBar';

class Portal extends React.Component {
  constructor(props) {
    super(props);

    this.root = document.querySelector('#root');
    this.onFocus = this.onFocus.bind(this);
  }

  onFocus() {
    const { id, isActive, onFocus } = this.props;
    return !isActive && onFocus(id);
  }

  render() {
    const {
      id,
      title,
      children,
      icon,
      onClose,
      isActive,
      isVisible,
      zIndex
    } = this.props;

    const className = `guihandle-${id}`;
    const handle = `.${className}`;

    return ReactDOM.createPortal(
      <Draggable handle={handle} onMouseDown={this.onFocus}>
        <Panel isActive={isActive} isVisible={isVisible} zIndex={zIndex}>
          <TitleBar handle={className} icon={icon} onClose={onClose}>
            {title}
          </TitleBar>
          <div>{children}</div>
        </Panel>
      </Draggable>,
      this.root
    );
  }
}

Portal.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  icon: PropTypes.string,
  isVisible: PropTypes.bool,
  onClose: PropTypes.func,

  // These props are provided by the Provider.
  id: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  zIndex: PropTypes.number.isRequired,
  onFocus: PropTypes.func.isRequired
};

Portal.defaultProps = {
  icon: null,
  isVisible: true,
  onClose: null
};

export default Portal;
