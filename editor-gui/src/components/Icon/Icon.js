import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({ name, ...props }) => {
  const className = `mdi mdi-${name}`;
  return <i className={className} {...props} />;
};

Icon.propTypes = {
  name: PropTypes.string.isRequired
};

export default Icon;
