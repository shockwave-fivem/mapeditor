/* eslint-disable operator-linebreak */
export const IS_BROWSER =
  !window.GetParentResourceName;

export const RESOURCE_NAME =
  window.GetParentResourceName
    ? window.GetParentResourceName()
    : 'editor-gui';
